#!/bin/bash

if ! diff -u <(sort /etc/passwd) <(./sort.py /etc/passwd); then
    echo "sort test failed on $path!"
    exit 1
fi

if ! diff -u <(seq 20 | sort) <(seq 20 | ./sort.py); then
    echo "sort test failed on implicit stdin!"
    exit 1
fi

if ! diff -u <(seq 20 | sort -) <(seq 20 | ./sort.py -); then
    echo "sort test failed on explicit stdin!"
    exit 1
fi

if ! diff -u <(seq 20 | sort -r) <(seq 20 | ./sort.py -r); then
    echo "sort test failed on reverse!"
    exit 1
fi

echo "sort test successful!"

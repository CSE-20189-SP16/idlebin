#!/bin/bash

for path in /etc/hosts /etc/passwd; do
    if ! diff -u <(cat $path) <(./cat.py $path); then
	echo "cat test failed on $path!"
	exit 1
    fi
done

if ! diff -u <(echo hello | cat) <(echo hello | ./cat.py); then
    echo "cat test failed on implicit stdin!"
    exit 1
fi

if ! diff -u <(echo hello | cat -) <(echo hello | ./cat.py -); then
    echo "cat test failed on explicit!"
    exit 1
fi

echo "cat test successful!"

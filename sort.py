#!/usr/bin/env python2.7

import getopt
import os
import sys

# Globals

REVERSE = False

# Usage function

def usage(status=0):
    print '''usage: {} [-r] files...

    -r reverse the result of comparisons'''.format(os.path.basename(sys.argv[0]))
    sys.exit(status)

# Parse command line options

try:
    opts, args = getopt.getopt(sys.argv[1:], "hr")
except getopt.GetoptError as e:
    print e
    usage(1)

for o, a in opts:
    if o == '-r':
        REVERSE = True
    else:
        usage(1)

if len(args) == 0:
    args.append('-')

# Main execution

for path in args:
    if path == '-':
        stream = sys.stdin
    else:
        stream = open(path)

    for line in sorted(stream, reverse=REVERSE):
        print line,

    stream.close()

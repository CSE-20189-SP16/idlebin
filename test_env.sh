#!/bin/bash

if ! diff -u <(env | grep -v "^_=" | sort) <(./env.py | grep -v "^_=" | sort); then
    echo "env test failed!"
    exit 1
fi

echo "env test successful!"

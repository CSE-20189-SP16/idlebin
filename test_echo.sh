#!/bin/bash

if ! diff -u <(echo a b c d) <(./echo.py a b c d); then
    echo "echo test failed (1)!"
    exit 1
fi

if ! diff -u <(echo a b    c d) <(./echo.py a b    c d); then
    echo "echo test failed (2)!"
    exit 1
fi

if ! diff -u <(echo "a b    c d") <(./echo.py "a b    c d"); then
    echo "echo test failed (3)!"
    exit 1
fi

echo "echo test successful!"

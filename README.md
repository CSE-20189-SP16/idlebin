Idlebin
=======

A partial re-implementation of standard Unix utilities in [Python]

- [cat](http://man7.org/linux/man-pages/man1/cat.1.html)

- [cp](http://man7.org/linux/man-pages/man1/cp.1.html)

- [echo](http://man7.org/linux/man-pages/man1/echo.1.html)

- [env](http://man7.org/linux/man-pages/man1/env.1.html)

- [sort](http://man7.org/linux/man-pages/man1/sort.1.html)

[Python]: https://www.python.org/

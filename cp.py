#!/usr/bin/env python2.7

import getopt
import os
import sys

# Global Variables

PROGRAM_NAME = os.path.basename(sys.argv[0])
BLOCKSIZE    = 4096
FORCE        = False

# Functions

def error(message, exit_code=1):
    print >>sys.stderr, message
    sys.exit(exit_code)

def usage(exit_code=0):
    error('''Usage: {} [-f] src dst

Options:

    -f          Overwrite existing file'''
    .format(PROGRAM_NAME), exit_code)

def open_fd(path, mode):
    try:
        return os.open(path, mode)
    except OSError as e:
        error('Could not open {}: {}'.format(SOURCE, e))

def read_fd(fd, n):
    try:
        return os.read(fd, n)
    except OSError as e:
        error('Could not read {} bytes from FD {}: {}'.format(n, fd, e))

def write_fd(fd, data):
    try:
        return os.write(fd, data)
    except OSError as e:
        error('Could not write {} bytes from FD {}: {}'.format(len(data), fd, e))

# Parse Command line arguments

try:
    options, arguments = getopt.getopt(sys.argv[1:], "hf")
except getopt.GetoptError as e:
    error(e)

for option, value in options:
    if option == '-h':
        usage(0)
    elif option == '-f':
        FORCE = True
    else:
        usage(1)

if len(arguments) != 2:
    usage(1)

SOURCE = arguments[0]
TARGET = arguments[1]

if os.path.exists(TARGET) and not FORCE:
    error('{} exists!'.format(TARGET))

# Main Execution

source = open_fd(SOURCE, os.O_RDONLY)
target = open_fd(TARGET, os.O_WRONLY|os.O_CREAT|os.O_TRUNC)

data = read_fd(source, BLOCKSIZE)
while data:
    write_fd(target, data)
    data = read_fd(source, BLOCKSIZE)

os.close(source)
os.close(target)

#!/usr/bin/env python2.7

import getopt
import os
import sys

# Global Variables

PROGRAM_NAME = os.path.basename(sys.argv[0])

# Functions

def error(message, exit_code=1):
    print >>sys.stderr, message
    sys.exit(exit_code)

def usage(exit_code=0):
    error('''Usage: {}

Options:

    -h     Show this help message'''
    .format(PROGRAM_NAME), exit_code)

# Parse Command line arguments

try:
    options, arguments = getopt.getopt(sys.argv[1:], "h")
except getopt.GetoptError as e:
    error(e)

for option, value in options:
    if option == '-h':
        usage(0)
    else:
        usage(1)

# Main Execution

error('{} Not implemented!'.format(PROGRAM_NAME))
